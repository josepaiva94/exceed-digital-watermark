package pt.up.fc.dcc.exceedw.utils;

import org.apache.commons.lang3.RandomStringUtils;

import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Base64;

/**
 * Encrypt/decrypt byte sequence. Uses 128-bit AES Password-Based Encryption.
 *
 * @author José Carlos Paiva    <josepaiva94@gmail.com>
 * @author Adélcia Fernandes    <up201308701@fc.up.pt>
 * @author Francisco Sucena     <up201102753@fc.up.pt>
 * @author Pedro Seruca         <up201109026@fc.up.pt>
 */
public class CryptoUtils {
    private static final String ENCRYPTION_ALGORITHM   = "AES";
    private static final String SECRET_KEY_ALGORITHM   = "PBKDF2WithHmacSHA256";
    private static final String ENCRYPTION_AES_MODE    = "AES/CBC/PKCS5Padding";
    private static final String SEP                    = "#";
    private static final int KEY_DERIVATION_ITER_COUNT = 65536;
    private static final int KEY_SIZE                  = 128;
    private static final int SALT_SIZE                 = 16;
    private static final int MIN_PASSWORD_SIZE         = 10;
    private static final int MAX_PASSWORD_SIZE         = 20;



    /**
     * Encrypt byte sequence
     * @param data byte sequence
     * @return encrypted byte sequence
     */
    public static CryptoResult encrypt(byte[] data) throws GeneralSecurityException {

        SecretKey secretKey = getSecret();

        Cipher cipher = Cipher.getInstance(ENCRYPTION_AES_MODE);
        cipher.init(Cipher.ENCRYPT_MODE, secretKey);
        AlgorithmParameters params = cipher.getParameters();
        byte[] iv = params.getParameterSpec(IvParameterSpec.class).getIV();
        byte[] cipherData = cipher.doFinal(data);

        // join IV and secret key (WARNING: this should not be done)
        CryptoResult result = new CryptoResult();
        result.setSecretCode(Base64.getEncoder().encodeToString(secretKey.getEncoded())
                + SEP + Base64.getEncoder().encodeToString(iv));
        result.setEncryptedData(cipherData);

        return result;
    }

    /**
     * Decrypt byte sequence
     * @param data encrypted byte sequence
     * @param secretCode secret code including secret key and IV
     * @return byte sequence
     */
    public static byte[] decrypt(byte[] data, String secretCode) throws GeneralSecurityException {

        String[] keys = secretCode.split(SEP);

        byte[] decodedKey = Base64.getDecoder().decode(keys[0]);
        SecretKey secretKey = new SecretKeySpec(decodedKey, 0, decodedKey.length, ENCRYPTION_ALGORITHM);

        byte[] iv = Base64.getDecoder().decode(keys[1]);

        Cipher cipher = Cipher.getInstance(ENCRYPTION_AES_MODE);
        cipher.init(Cipher.DECRYPT_MODE, secretKey, new IvParameterSpec(iv));

        return cipher.doFinal(data);
    }

    /**
     * Get a secret key
     * @return secret key
     */
    private static SecretKey getSecret() throws NoSuchAlgorithmException, InvalidKeySpecException {
        String password = generatePassword();
        byte[] salt = generateSalt();

        SecretKeyFactory factory = SecretKeyFactory.getInstance(SECRET_KEY_ALGORITHM);
        KeySpec spec = new PBEKeySpec(password.toCharArray(), salt, KEY_DERIVATION_ITER_COUNT, KEY_SIZE);
        SecretKey tmp = factory.generateSecret(spec);

        return new SecretKeySpec(tmp.getEncoded(), ENCRYPTION_ALGORITHM);
    }

    /**
     * Generate a random password
     * @return random password
     */
    private static String generatePassword() {
        return RandomStringUtils.randomAlphanumeric(MIN_PASSWORD_SIZE, MAX_PASSWORD_SIZE);
    }

    /**
     * Generate a random salt
     * @return random salt
     */
    private static byte[] generateSalt() {
        SecureRandom random = new SecureRandom();
        byte salt[] = new byte[SALT_SIZE];
        random.nextBytes(salt);

        return salt;
    }


    /**
     * Result of encryption
     */
    public static class CryptoResult {
        private byte[] encryptedData;
        private String secretCode;

        public CryptoResult() {
        }

        public byte[] getEncryptedData() {
            return encryptedData;
        }

        public void setEncryptedData(byte[] encryptedData) {
            this.encryptedData = encryptedData;
        }

        public String getSecretCode() {
            return secretCode;
        }

        public void setSecretCode(String secretCode) {
            this.secretCode = secretCode;
        }
    }
}
