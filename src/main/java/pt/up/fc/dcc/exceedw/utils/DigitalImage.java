package pt.up.fc.dcc.exceedw.utils;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;

/**
 * Contains facilities for loading, displaying, saving and managing images
 *
 * @author José Carlos Paiva    <josepaiva94@gmail.com>
 * @author Adélcia Fernandes    <up201308701@fc.up.pt>
 * @author Francisco Sucena     <up201102753@fc.up.pt>
 * @author Pedro Seruca         <up201109026@fc.up.pt>
 */
public class DigitalImage {
    public static final String ALLOWED_IMAGE_OUTPUT = "png";

    private JFrame frame;
    private File imageFile;
    private BufferedImage bufferedImage;

    public DigitalImage(File imageFile) throws IOException {
        this.imageFile = imageFile;
        bufferedImage = ImageIO.read(imageFile);
    }

    public DigitalImage(BufferedImage bufferedImage) throws IOException {
        this.bufferedImage = bufferedImage;
        imageFile = new File("result.png");
    }

    /**
     * Width of the image
     * @return width of the image
     */
    public int getWidth() {
        return bufferedImage.getWidth();
    }

    /**
     * Height of the image
     * @return height of the image
     */
    public int getHeight() {
        return bufferedImage.getHeight();
    }

    /**
     * Get buffered image
     * @return buffered image
     */
    public BufferedImage getBufferedImage() {
        return bufferedImage;
    }

    /**
     * Get byte array of the image
     * @return byte array of the image
     * @throws IOException
     */
    public byte[] toByteArray() throws IOException {

        if (bufferedImage == null)
            return null;

        WritableRaster raster = bufferedImage.getRaster();
        DataBufferByte data = (DataBufferByte) raster.getDataBuffer();

        return data.getData();
    }

    /**
     * Popup image in a JFrame
     */
    public void popup() {

        if (frame != null) {
            frame.setVisible(true);
            frame.repaint();
            return;
        }

        JMenuBar menuBar = new JMenuBar();
        JMenu menu = new JMenu("Image");
        JMenuItem saveItem = new JMenuItem("Save as ...");
        saveItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                save();
            }
        });
        menu.add(saveItem);
        menuBar.add(menu);

        frame = new JFrame();
        frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        frame.setTitle(imageFile.getName());
        frame.setResizable(false);
        frame.setVisible(true);

        frame.setJMenuBar(menuBar);
        frame.setContentPane(getImageLabel());

        frame.pack();
        frame.repaint();
    }

    /**
     * Show image in label lbl
     *
     * @param lbl
     */
    public void show(JLabel lbl) {
        if (lbl == null || bufferedImage == null)
            return;

        lbl.setIcon(new ImageIcon(bufferedImage.getScaledInstance(
                lbl.getWidth(), lbl.getHeight(),
                BufferedImage.TRANSLUCENT)));
    }

    /**
     * Get a label with the image of the size of the image
     */
    public JLabel getImageLabel() {
        JLabel lbl = new JLabel();
        lbl.setSize(bufferedImage.getWidth(), bufferedImage.getHeight());
        show(lbl);

        return lbl;
    }

    /**
     * Save a buffered image in a specified location (prompts the user for location)
     */
    public void save() {
        final JFileChooser fileChooser = new JFileChooser();
        fileChooser.setEnabled(false);
        FileFilter filter = new FileNameExtensionFilter(ALLOWED_IMAGE_OUTPUT.toUpperCase() + " Images",
                ALLOWED_IMAGE_OUTPUT);
        fileChooser.setFileFilter(filter);
        fileChooser.setBounds(0, -41, 582, 397);
        fileChooser.setDialogType(JFileChooser.SAVE_DIALOG);
        fileChooser.setCurrentDirectory(Paths.get("src", "main", "resources", "generated").toFile());

        final int userValue = fileChooser.showOpenDialog(fileChooser);
        if (userValue == JFileChooser.APPROVE_OPTION) {
            imageFile = fileChooser.getSelectedFile();

            if (imageFile == null)
                return;

            String filename = imageFile.getName();
            String extension = filename.substring(filename.lastIndexOf(".") + 1).toLowerCase();

            if (!ALLOWED_IMAGE_OUTPUT.contains(extension)) {
                Alerts.showErrorMsg("Output extension not allowed");
                return;
            }

            try {
                ImageIO.write(bufferedImage, extension, imageFile);
            } catch (IOException e) {
                Alerts.showErrorMsg(e.getMessage());
                e.printStackTrace();
            }
        }
    }
}
