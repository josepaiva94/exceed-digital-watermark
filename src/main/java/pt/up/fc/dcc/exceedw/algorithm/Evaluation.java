package pt.up.fc.dcc.exceedw.algorithm;

import pt.up.fc.dcc.exceedw.utils.DigitalImage;

import java.awt.image.BufferedImage;

/**
 * Interface for evaluation metrics of the watermarking algorithms
 *
 * @author José Carlos Paiva    <josepaiva94@gmail.com>
 * @author Adélcia Fernandes    <up201308701@fc.up.pt>
 * @author Francisco Sucena     <up201102753@fc.up.pt>
 * @author Pedro Seruca         <up201109026@fc.up.pt>
 */
public interface Evaluation {

    /**
     * Evaluate and retrieve performance in this metric of an algorithm
     * @return performance in this metric of the algorithm
     * @throws EvaluationException
     */
    void evaluate() throws EvaluationException;
}
