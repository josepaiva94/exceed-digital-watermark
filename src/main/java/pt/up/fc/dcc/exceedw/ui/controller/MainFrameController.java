package pt.up.fc.dcc.exceedw.ui.controller;

import pt.up.fc.dcc.exceedw.algorithm.*;
import pt.up.fc.dcc.exceedw.algorithm.Algorithm.EmbedResult;
import pt.up.fc.dcc.exceedw.algorithm.Algorithm.ExtractResult;
import pt.up.fc.dcc.exceedw.ui.view.MainFrame;
import pt.up.fc.dcc.exceedw.utils.Alerts;
import pt.up.fc.dcc.exceedw.utils.DigitalImage;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;

/**
 * Controller of the main form
 *
 * @author José Carlos Paiva    <josepaiva94@gmail.com>
 * @author Adélcia Fernandes    <up201308701@fc.up.pt>
 * @author Francisco Sucena     <up201102753@fc.up.pt>
 * @author Pedro Seruca         <up201109026@fc.up.pt>
 */
public class MainFrameController {

    private LSB lsb = new LSB();

    private MainFrame mainFrame;

    // view panels
    private JPanel initialPanel;
    private JPanel insertPanel;
    private JPanel extractPanel;
    private JPanel comparePanel;

    // Initial panel components
    private JButton insertBtn;
    private JButton extractBtn;
    private JButton compareBtn;

    // Insert watermark panel components
    private JButton selectImageBtn;
    private JLabel imageLbl;
    private JPanel msgWatermarkPnl;
    private JPanel imgWatermarkPnl;
    private JButton selectWatermarkBtn;
    private JLabel watermarkLbl;
    private JButton insertWatermarkBtn;
    private JTextField secretTxtBox;
    private JRadioButton textWatermarkRb;
    private JRadioButton imageWatermarkRb;
    private JTextField messageWatermarkTxt;
    private JButton insertBackBtn;

    private DigitalImage encryptImage;
    private DigitalImage encryptWatermark;

    // Extract watermark panel components
    private JLabel encryptedImageLbl;
    private JButton encryptedImageBtn;
    private JTextField extractSecretCodeTxtBox;
    private JButton extractWatermarkBtn;
    private JButton extractBackBtn;

    private DigitalImage decryptImage;

    // Compare images panel components
    private JLabel cmpImage1Lbl;
    private JLabel cmpImage2Lbl;
    private JButton cmpSelectImage1Btn;
    private JButton cmpSelectImage2Btn;
    private JLabel mseLbl;
    private JLabel snrLbl;
    private JLabel psnrLbl;
    private JButton compareImagesBtn;
    private JButton cmpBackBtn;

    private DigitalImage cmpImage1;
    private DigitalImage cmpImage2;


    public MainFrameController() {
        initComponents();
        initListeners();
    }

    /**
     * Initialize components
     */
    private void initComponents() {
        mainFrame = new MainFrame();

        initialPanel = mainFrame.getInitialPanel();
        insertPanel = mainFrame.getInsertPanel();
        extractPanel = mainFrame.getExtractPanel();
        comparePanel = mainFrame.getComparePanel();

        insertBtn = mainFrame.getInsertBtn();
        extractBtn = mainFrame.getExtractBtn();
        compareBtn = mainFrame.getCompareBtn();

        selectImageBtn = mainFrame.getSelectImageBtn();
        imageLbl = mainFrame.getImageLbl();
        msgWatermarkPnl = mainFrame.getMsgWatermarkPnl();
        imgWatermarkPnl = mainFrame.getImgWatermarkPnl();
        selectWatermarkBtn = mainFrame.getSelectWatermarkBtn();
        watermarkLbl = mainFrame.getWatermarkLbl();
        insertWatermarkBtn = mainFrame.getInsertWatermarkBtn();
        secretTxtBox = mainFrame.getSecretTxtBox();
        textWatermarkRb = mainFrame.getTextWatermarkRb();
        imageWatermarkRb = mainFrame.getImageWatermarkRb();
        messageWatermarkTxt = mainFrame.getMessageWatermarkTxt();
        insertBackBtn = mainFrame.getInsertBackBtn();

        encryptedImageLbl = mainFrame.getEncryptedImageLbl();
        encryptedImageBtn = mainFrame.getEncryptedImageBtn();
        extractSecretCodeTxtBox = mainFrame.getExtractSecretCodeTxtBox();
        extractWatermarkBtn = mainFrame.getExtractWatermarkBtn();
        extractBackBtn = mainFrame.getExtractBackBtn();

        cmpImage1Lbl = mainFrame.getCmpImage1Lbl();
        cmpImage2Lbl = mainFrame.getCmpImage2Lbl();
        cmpSelectImage1Btn = mainFrame.getCmpSelectImage1Btn();
        cmpSelectImage2Btn = mainFrame.getCmpSelectImage2Btn();
        mseLbl = mainFrame.getMseLbl();
        snrLbl = mainFrame.getSnrLbl();
        psnrLbl = mainFrame.getPsnrLbl();
        compareImagesBtn = mainFrame.getCompareImagesBtn();
        cmpBackBtn = mainFrame.getCmpBackBtn();
    }

    /**
     * Initialize listeners
     */
    private void initListeners() {
        insertBtn.addActionListener(new InsertBtnListener());
        extractBtn.addActionListener(new ExtractBtnListener());
        compareBtn.addActionListener(new CompareBtnListener());

        selectImageBtn.addActionListener(new EncryptSelectImageBtnListener());
        selectWatermarkBtn.addActionListener(new EncryptSelectWatermarkBtnListener());
        messageWatermarkTxt.getDocument().addDocumentListener(new EncryptWatermarkTxtBoxListener());
        textWatermarkRb.addActionListener(new WatermarkRbListener());
        imageWatermarkRb.addActionListener(new WatermarkRbListener());
        insertWatermarkBtn.addActionListener(new InsertWatermarkBtnListener());
        insertBackBtn.addActionListener(new BackBtnListener());

        encryptedImageBtn.addActionListener(new DecryptSelectImageBtnListener());
        extractSecretCodeTxtBox.getDocument().addDocumentListener(new DecryptSecretTxtBoxListener());
        extractWatermarkBtn.addActionListener(new ExtractWatermarkBtnListener());
        extractBackBtn.addActionListener(new BackBtnListener());

        cmpSelectImage1Btn.addActionListener(new CompareSelectImg1BtnListener());
        cmpSelectImage2Btn.addActionListener(new CompareSelectImg2BtnListener());
        compareImagesBtn.addActionListener(new CompareImagesBtnListener());
        cmpBackBtn.addActionListener(new BackBtnListener());
    }

    /**
     * Show main frame window
     */
    public void showMainFrameWindow() {
        mainFrame.setVisible(true);
    }

    /**
     * Hide main frame window
     */
    public void hideMainFrameWindow() {
        mainFrame.setVisible(false);
    }

    /**
     * Listener class for embed button
     */
    private class InsertBtnListener implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            initialPanel.setVisible(false);
            insertPanel.setVisible(true);
        }
    }

    /**
     * Listener class for extract button
     */
    private class ExtractBtnListener implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            initialPanel.setVisible(false);
            extractPanel.setVisible(true);
        }
    }

    /**
     * Listener class for compare button
     */
    private class CompareBtnListener implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            initialPanel.setVisible(false);
            comparePanel.setVisible(true);
        }
    }

    /**
     * Listener class for back button
     */
    private class BackBtnListener implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            initialPanel.setVisible(true);
            insertPanel.setVisible(false);
            extractPanel.setVisible(false);
            comparePanel.setVisible(false);

            // clear all fields
            encryptImage = encryptWatermark = decryptImage = cmpImage1 = cmpImage2 = null;
            imageLbl.setIcon(null);
            watermarkLbl.setIcon(null);
            encryptedImageLbl.setIcon(null);
            secretTxtBox.setText("");
            messageWatermarkTxt.setText("");
            extractSecretCodeTxtBox.setText("");
            insertWatermarkBtn.setEnabled(false);
            extractWatermarkBtn.setEnabled(false);
            cmpImage1Lbl.setIcon(null);
            cmpImage2Lbl.setIcon(null);
            mseLbl.setText("-");
            snrLbl.setText("-");
            psnrLbl.setText("-");
        }
    }

    /**
     * Listener class for selecting an image
     */
    private abstract class SelectImageListener implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            selectImage();
        }

        /**
         * Displays the file chooser for selecting an image
         */
        public void selectImage() {

            final JFileChooser fileChooser = new JFileChooser();
            fileChooser.setEnabled(false);
            FileFilter filter = new FileNameExtensionFilter("PNG Images", "png");
            fileChooser.setFileFilter(filter);
            fileChooser.setBounds(0, -41, 582, 397);
            fileChooser.setDialogType(JFileChooser.OPEN_DIALOG);
            fileChooser.setCurrentDirectory(Paths.get("src", "main", "resources").toFile());

            final int userValue = fileChooser.showOpenDialog(fileChooser);
            if (userValue == JFileChooser.APPROVE_OPTION) {
                File image = fileChooser.getSelectedFile();

                if (image == null)
                    return;

                try {
                    setSelectedImage(new DigitalImage(image));
                } catch (IOException e) {
                    Alerts.showErrorMsg(e.getMessage());
                    e.printStackTrace();
                }
            }
        }

        /**
         * Sets the currently selected image and displays it in its label
         * @param image
         */
        public abstract void setSelectedImage(DigitalImage image);
    }

    /**
     * Listener class for selecting an image in the embed panel
     */
    private class EncryptSelectImageBtnListener extends SelectImageListener {

        @Override
        public void setSelectedImage(DigitalImage image) {

            encryptImage = image;
            image.show(imageLbl);

            if (encryptWatermark != null && imageWatermarkRb.isSelected()
                    || !messageWatermarkTxt.getText().isEmpty() && textWatermarkRb.isSelected())
                insertWatermarkBtn.setEnabled(true);
        }
    }

    /**
     * Listener class for selecting a watermark in the embed panel
     */
    private class EncryptSelectWatermarkBtnListener extends SelectImageListener {

        @Override
        public void setSelectedImage(DigitalImage image) {

            encryptWatermark = image;
            image.show(watermarkLbl);

            if (encryptImage != null)
                insertWatermarkBtn.setEnabled(true);
        }
    }

    /**
     * Listener class for changes in watermark text box in the embed panel
     */
    private class EncryptWatermarkTxtBoxListener implements DocumentListener {

        public void insertUpdate(DocumentEvent e) {
            if (encryptImage != null)
                insertWatermarkBtn.setEnabled(true);
        }

        public void removeUpdate(DocumentEvent e) {
            if (messageWatermarkTxt.getText().isEmpty())
                insertWatermarkBtn.setEnabled(false);
        }

        public void changedUpdate(DocumentEvent e) {

        }
    }

    /**
     * Listener class for selecting an encrypted image in the extract panel
     */
    private class DecryptSelectImageBtnListener extends SelectImageListener {

        @Override
        public void setSelectedImage(DigitalImage image) {

            decryptImage = image;
            image.show(encryptedImageLbl);

            if (!extractSecretCodeTxtBox.getText().isEmpty())
                extractWatermarkBtn.setEnabled(true);
        }
    }

    /**
     * Listener class for changes in secret text box in the extract panel
     */
    private class DecryptSecretTxtBoxListener implements DocumentListener {

        public void insertUpdate(DocumentEvent e) {
            extractWatermarkBtn.setEnabled(true);
        }

        public void removeUpdate(DocumentEvent e) {
            if (extractSecretCodeTxtBox.getText().isEmpty())
                extractWatermarkBtn.setEnabled(false);
        }

        public void changedUpdate(DocumentEvent e) {

        }
    }

    /**
     * Listener class for change in watermark type
     */
    private class WatermarkRbListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {

            boolean text = textWatermarkRb.isSelected();
            msgWatermarkPnl.setVisible(text);
            imgWatermarkPnl.setVisible(!text);

            if (text) {
                if (encryptImage != null && !messageWatermarkTxt.getText().isEmpty())
                    insertWatermarkBtn.setEnabled(true);
                else
                    insertWatermarkBtn.setEnabled(false);
            } else {
                if (encryptImage != null && encryptWatermark != null)
                    insertWatermarkBtn.setEnabled(true);
                else
                    insertWatermarkBtn.setEnabled(false);
            }
        }
    }

    /**
     * Listener class for insert watermark button
     */
    private class InsertWatermarkBtnListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {

            boolean text = textWatermarkRb.isSelected();

            EmbedResult result;
            if (text) {
                try {
                    result = lsb.embed(encryptImage, messageWatermarkTxt.getText());
                } catch (EmbedException ee) {
                    Alerts.showErrorMsg(ee.getMessage());
                    return;
                }
            } else {
                try {
                    result = lsb.embed(encryptImage, encryptWatermark);
                } catch (EmbedException ee) {
                    Alerts.showErrorMsg(ee.getMessage());
                    return;
                }
            }

            secretTxtBox.setText(result.getSecretCode());

            result.getImage().popup();
        }
    }

    /**
     * Listener class for extract watermark button
     */
    private class ExtractWatermarkBtnListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {

            ExtractResult result;
            try {
                result = lsb.extract(decryptImage, extractSecretCodeTxtBox.getText());
            } catch (ExtractException ee) {
                Alerts.showErrorMsg(ee.getMessage());
                return;
            }

            if (result.returnTextOrImage() instanceof DigitalImage) {
                ((DigitalImage) result
                        .returnTextOrImage()).popup();
            } else {
                Alerts.showMsg(result.returnTextOrImage(), "Watermark");
            }
        }
    }

    /**
     * Listener class for button to select first image to compare
     */
    private class CompareSelectImg1BtnListener extends SelectImageListener {

        @Override
        public void setSelectedImage(DigitalImage image) {

            cmpImage1 = image;
            image.show(cmpImage1Lbl);

            if (cmpImage2 != null)
                compareImagesBtn.setEnabled(true);
        }
    }

    /**
     * Listener class for button to select second image to compare
     */
    private class CompareSelectImg2BtnListener extends SelectImageListener {

        @Override
        public void setSelectedImage(DigitalImage image) {

            cmpImage2 = image;
            image.show(cmpImage2Lbl);

            if (cmpImage1 != null)
                compareImagesBtn.setEnabled(true);
        }
    }

    /**
     * Listener class for compare images button
     */
    private class CompareImagesBtnListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {

            if (cmpImage1 == null || cmpImage2 == null)
                return;

            PSNR psnrEvaluation = new PSNR(cmpImage1.getBufferedImage(), cmpImage2.getBufferedImage());
            try {
                psnrEvaluation.evaluate();
                mseLbl.setText(String.format("%7.4f", psnrEvaluation.getMse()));
                snrLbl.setText(String.format("%7.4f", psnrEvaluation.getSnr()));
                psnrLbl.setText(String.format("%7.4f", psnrEvaluation.getPsnr()));
            } catch (EvaluationException e1) {
                Alerts.showErrorMsg(e1.getMessage());
                e1.printStackTrace();
            }
        }
    }
}
