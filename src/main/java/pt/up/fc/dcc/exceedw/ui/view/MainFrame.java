package pt.up.fc.dcc.exceedw.ui.view;

import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;

import javax.swing.*;
import java.awt.*;

/**
 * Main frame of the UI
 *
 * @author José Carlos Paiva    <josepaiva94@gmail.com>
 * @author Adélcia Fernandes    <up201308701@fc.up.pt>
 * @author Francisco Sucena     <up201102753@fc.up.pt>
 * @author Pedro Seruca         <up201109026@fc.up.pt>
 */
public class MainFrame extends JFrame {

    private static final int WIDTH = 500;
    private static final int HEIGHT = 500;
    private static final String TITLE = "ExceeDW";

    // parent panel
    private JPanel mainPanel;

    // view panels
    private JPanel initialPanel;
    private JPanel insertPanel;
    private JPanel extractPanel;
    private JPanel comparePanel;

    // initial panel components
    private JPanel titlePanel;
    private JPanel commandsPanel;
    private JButton insertBtn;
    private JButton extractBtn;
    private JButton compareBtn;

    // insert panel components
    private JButton selectImageBtn;
    private JButton selectWatermarkBtn;
    private JLabel imageLbl;
    private JLabel watermarkLbl;
    private JPanel msgWatermarkPnl;
    private JPanel imgWatermarkPnl;
    private JButton insertWatermarkBtn;
    private JTextField secretTxtBox;
    private JRadioButton textWatermarkRb;
    private JRadioButton imageWatermarkRb;
    private JTextField messageWatermarkTxt;
    private JButton insertBackBtn;

    // extract panel components
    private JLabel encryptedImageLbl;
    private JButton encryptedImageBtn;
    private JTextField extractSecretCodeTxtBox;
    private JButton extractWatermarkBtn;
    private JButton extractBackBtn;

    // compare panel components
    private JLabel cmpImage1Lbl;
    private JLabel cmpImage2Lbl;
    private JButton cmpSelectImage1Btn;
    private JButton cmpSelectImage2Btn;
    private JButton cmpBackBtn;
    private JLabel mseLbl;
    private JLabel snrLbl;
    private JLabel psnrLbl;
    private JButton compareImagesBtn;


    public MainFrame() {
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setTitle(TITLE);
        setSize(WIDTH, HEIGHT);
        setResizable(false);
        setContentPane(mainPanel);
        setLocationByPlatform(true);
        setLocationRelativeTo(null);

        titlePanel.setLayout(new BoxLayout(titlePanel, BoxLayout.Y_AXIS));
        commandsPanel.setLayout(new BoxLayout(commandsPanel, BoxLayout.Y_AXIS));
    }

    public JPanel getMainPanel() {
        return mainPanel;
    }

    public JPanel getInitialPanel() {
        return initialPanel;
    }

    public JPanel getInsertPanel() {
        return insertPanel;
    }

    public JPanel getComparePanel() {
        return comparePanel;
    }

    public JButton getInsertBtn() {
        return insertBtn;
    }

    public JButton getExtractBtn() {
        return extractBtn;
    }

    public JButton getCompareBtn() {
        return compareBtn;
    }

    public JButton getSelectImageBtn() {
        return selectImageBtn;
    }

    public JButton getSelectWatermarkBtn() {
        return selectWatermarkBtn;
    }

    public JLabel getImageLbl() {
        return imageLbl;
    }

    public JPanel getMsgWatermarkPnl() {
        return msgWatermarkPnl;
    }

    public JPanel getImgWatermarkPnl() {
        return imgWatermarkPnl;
    }

    public JLabel getWatermarkLbl() {
        return watermarkLbl;
    }

    public JButton getInsertWatermarkBtn() {
        return insertWatermarkBtn;
    }

    public JTextField getSecretTxtBox() {
        return secretTxtBox;
    }

    public JRadioButton getTextWatermarkRb() {
        return textWatermarkRb;
    }

    public JRadioButton getImageWatermarkRb() {
        return imageWatermarkRb;
    }

    public JTextField getMessageWatermarkTxt() {
        return messageWatermarkTxt;
    }

    public JButton getInsertBackBtn() {
        return insertBackBtn;
    }

    public JPanel getExtractPanel() {
        return extractPanel;
    }

    public JLabel getEncryptedImageLbl() {
        return encryptedImageLbl;
    }

    public JButton getEncryptedImageBtn() {
        return encryptedImageBtn;
    }

    public JButton getExtractWatermarkBtn() {
        return extractWatermarkBtn;
    }

    public JTextField getExtractSecretCodeTxtBox() {
        return extractSecretCodeTxtBox;
    }

    public JButton getExtractBackBtn() {
        return extractBackBtn;
    }

    public JLabel getCmpImage1Lbl() {
        return cmpImage1Lbl;
    }

    public JLabel getCmpImage2Lbl() {
        return cmpImage2Lbl;
    }

    public JButton getCmpSelectImage1Btn() {
        return cmpSelectImage1Btn;
    }

    public JButton getCmpSelectImage2Btn() {
        return cmpSelectImage2Btn;
    }

    public JButton getCmpBackBtn() {
        return cmpBackBtn;
    }

    public JLabel getMseLbl() {
        return mseLbl;
    }

    public JLabel getSnrLbl() {
        return snrLbl;
    }

    public JLabel getPsnrLbl() {
        return psnrLbl;
    }

    public JButton getCompareImagesBtn() {
        return compareImagesBtn;
    }

    {
// GUI initializer generated by IntelliJ IDEA GUI Designer
// >>> IMPORTANT!! <<<
// DO NOT EDIT OR ADD ANY CODE HERE!
        $$$setupUI$$$();
    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     *
     * @noinspection ALL
     */
    private void $$$setupUI$$$() {
        final JPanel panel1 = new JPanel();
        panel1.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
        panel1.setMinimumSize(new Dimension(500, 500));
        mainPanel = new JPanel();
        mainPanel.setLayout(new CardLayout(5, 5));
        mainPanel.setMinimumSize(new Dimension(350, 350));
        panel1.add(mainPanel, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        initialPanel = new JPanel();
        initialPanel.setLayout(new GridLayoutManager(2, 1, new Insets(0, 0, 0, 0), -1, -1));
        mainPanel.add(initialPanel, "initial");
        titlePanel = new JPanel();
        titlePanel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
        initialPanel.add(titlePanel, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, 1, null, new Dimension(207, 157), new Dimension(-1, 100), 0, false));
        final JLabel label1 = new JLabel();
        label1.setFont(new Font(label1.getFont().getName(), Font.BOLD | Font.ITALIC, 48));
        label1.setHorizontalAlignment(0);
        label1.setHorizontalTextPosition(0);
        label1.setMaximumSize(new Dimension(300, 56));
        label1.setMinimumSize(new Dimension(300, 56));
        label1.setText("ExceeDW");
        titlePanel.add(label1);
        final JLabel label2 = new JLabel();
        label2.setHorizontalAlignment(0);
        label2.setHorizontalTextPosition(0);
        label2.setMaximumSize(new Dimension(300, 18));
        label2.setMinimumSize(new Dimension(300, 18));
        label2.setText("A digital watermarking system");
        label2.setVerticalAlignment(0);
        titlePanel.add(label2);
        commandsPanel = new JPanel();
        commandsPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
        initialPanel.add(commandsPanel, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, 1, null, null, null, 0, false));
        final JPanel panel2 = new JPanel();
        panel2.setLayout(new GridLayoutManager(1, 1, new Insets(5, 0, 5, 0), -1, -1));
        commandsPanel.add(panel2);
        insertBtn = new JButton();
        insertBtn.setHorizontalAlignment(0);
        insertBtn.setHorizontalTextPosition(0);
        insertBtn.setText("Embed Watermark");
        panel2.add(insertBtn, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE, 1, GridConstraints.SIZEPOLICY_FIXED, new Dimension(250, -1), null, null, 0, false));
        final JPanel panel3 = new JPanel();
        panel3.setLayout(new GridLayoutManager(1, 1, new Insets(5, 0, 5, 0), -1, -1));
        commandsPanel.add(panel3);
        extractBtn = new JButton();
        extractBtn.setText("Extract Watermark");
        panel3.add(extractBtn, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE, 1, GridConstraints.SIZEPOLICY_FIXED, new Dimension(250, -1), null, null, 0, false));
        final JPanel panel4 = new JPanel();
        panel4.setLayout(new GridLayoutManager(1, 1, new Insets(5, 0, 5, 0), -1, -1));
        commandsPanel.add(panel4);
        compareBtn = new JButton();
        compareBtn.setText("Compare Images");
        panel4.add(compareBtn, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE, 1, GridConstraints.SIZEPOLICY_FIXED, new Dimension(250, -1), null, null, 0, false));
        insertPanel = new JPanel();
        insertPanel.setLayout(new GridLayoutManager(3, 1, new Insets(0, 0, 0, 0), -1, -1));
        mainPanel.add(insertPanel, "insert");
        final JPanel panel5 = new JPanel();
        panel5.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
        insertPanel.add(panel5, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, 1, null, null, new Dimension(-1, 75), 0, false));
        final JLabel label3 = new JLabel();
        label3.setFont(new Font(label3.getFont().getName(), Font.BOLD | Font.ITALIC, 36));
        label3.setHorizontalAlignment(0);
        label3.setHorizontalTextPosition(0);
        label3.setText("Insert Watermark");
        panel5.add(label3);
        final JPanel panel6 = new JPanel();
        panel6.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
        insertPanel.add(panel6, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        final JPanel panel7 = new JPanel();
        panel7.setLayout(new BorderLayout(0, 0));
        panel6.add(panel7, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        final JPanel panel8 = new JPanel();
        panel8.setLayout(new GridLayoutManager(2, 1, new Insets(0, 0, 15, 5), -1, -1, true, false));
        panel7.add(panel8, BorderLayout.WEST);
        imageLbl = new JLabel();
        imageLbl.setHorizontalAlignment(0);
        imageLbl.setHorizontalTextPosition(0);
        imageLbl.setText("");
        panel8.add(imageLbl, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, new Dimension(200, 200), null, null, 0, false));
        selectImageBtn = new JButton();
        selectImageBtn.setText("Select Image");
        panel8.add(selectImageBtn, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JPanel panel9 = new JPanel();
        panel9.setLayout(new GridLayoutManager(2, 1, new Insets(0, 0, 15, 0), -1, -1));
        panel7.add(panel9, BorderLayout.EAST);
        imgWatermarkPnl = new JPanel();
        imgWatermarkPnl.setLayout(new GridLayoutManager(2, 1, new Insets(0, 0, 0, 0), -1, -1));
        panel9.add(imgWatermarkPnl, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        watermarkLbl = new JLabel();
        watermarkLbl.setHorizontalAlignment(0);
        watermarkLbl.setHorizontalTextPosition(0);
        watermarkLbl.setText("");
        imgWatermarkPnl.add(watermarkLbl, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, new Dimension(200, 200), null, null, 0, false));
        selectWatermarkBtn = new JButton();
        selectWatermarkBtn.setText("Select Watermark");
        imgWatermarkPnl.add(selectWatermarkBtn, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        msgWatermarkPnl = new JPanel();
        msgWatermarkPnl.setLayout(new GridLayoutManager(1, 2, new Insets(0, 0, 0, 0), -1, -1));
        msgWatermarkPnl.setVisible(false);
        panel9.add(msgWatermarkPnl, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        final JLabel label4 = new JLabel();
        label4.setText("Message");
        msgWatermarkPnl.add(label4, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        messageWatermarkTxt = new JTextField();
        msgWatermarkPnl.add(messageWatermarkTxt, new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
        final JPanel panel10 = new JPanel();
        panel10.setLayout(new GridLayoutManager(3, 1, new Insets(0, 0, 0, 0), -1, -1));
        panel7.add(panel10, BorderLayout.SOUTH);
        final JPanel panel11 = new JPanel();
        panel11.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
        panel11.setVisible(true);
        panel10.add(panel11, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_EAST, GridConstraints.FILL_VERTICAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        final JLabel label5 = new JLabel();
        label5.setText("Type");
        panel11.add(label5);
        textWatermarkRb = new JRadioButton();
        textWatermarkRb.setText("Text");
        panel11.add(textWatermarkRb);
        imageWatermarkRb = new JRadioButton();
        imageWatermarkRb.setSelected(true);
        imageWatermarkRb.setText("Image");
        panel11.add(imageWatermarkRb);
        insertWatermarkBtn = new JButton();
        insertWatermarkBtn.setEnabled(false);
        insertWatermarkBtn.setText("Encrypt and Insert");
        panel10.add(insertWatermarkBtn, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JPanel panel12 = new JPanel();
        panel12.setLayout(new BorderLayout(5, 0));
        panel10.add(panel12, new GridConstraints(2, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        final JLabel label6 = new JLabel();
        label6.setText("Secret Code");
        panel12.add(label6, BorderLayout.WEST);
        secretTxtBox = new JTextField();
        secretTxtBox.setEditable(false);
        secretTxtBox.setMinimumSize(new Dimension(150, 26));
        secretTxtBox.setText("");
        secretTxtBox.setToolTipText("Save this code somewhere safe");
        panel12.add(secretTxtBox, BorderLayout.CENTER);
        final JPanel panel13 = new JPanel();
        panel13.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
        panel13.setBackground(new Color(-1052689));
        panel13.setRequestFocusEnabled(true);
        panel13.setVisible(true);
        insertPanel.add(panel13, new GridConstraints(2, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, 1, 1, null, null, null, 0, false));
        insertBackBtn = new JButton();
        insertBackBtn.setText("Back");
        insertBackBtn.setVisible(true);
        panel13.add(insertBackBtn, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_EAST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        extractPanel = new JPanel();
        extractPanel.setLayout(new GridLayoutManager(3, 1, new Insets(0, 0, 0, 0), -1, -1));
        mainPanel.add(extractPanel, "extract");
        final JPanel panel14 = new JPanel();
        panel14.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
        extractPanel.add(panel14, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, 1, null, null, new Dimension(-1, 75), 0, false));
        final JLabel label7 = new JLabel();
        label7.setFont(new Font(label7.getFont().getName(), Font.BOLD | Font.ITALIC, 36));
        label7.setText("Extract Watermark");
        panel14.add(label7);
        final JPanel panel15 = new JPanel();
        panel15.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
        extractPanel.add(panel15, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        final JPanel panel16 = new JPanel();
        panel16.setLayout(new GridLayoutManager(4, 1, new Insets(0, 0, 0, 0), -1, 5));
        panel15.add(panel16, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_VERTICAL, 1, 1, new Dimension(300, -1), null, null, 0, false));
        encryptedImageLbl = new JLabel();
        encryptedImageLbl.setHorizontalAlignment(0);
        encryptedImageLbl.setHorizontalTextPosition(0);
        encryptedImageLbl.setText("");
        panel16.add(encryptedImageLbl, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, new Dimension(300, 300), null, null, 0, false));
        encryptedImageBtn = new JButton();
        encryptedImageBtn.setText("Select Image");
        panel16.add(encryptedImageBtn, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JPanel panel17 = new JPanel();
        panel17.setLayout(new BorderLayout(5, 5));
        panel16.add(panel17, new GridConstraints(2, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        final JLabel label8 = new JLabel();
        label8.setText("Secret Code");
        panel17.add(label8, BorderLayout.WEST);
        extractSecretCodeTxtBox = new JTextField();
        panel17.add(extractSecretCodeTxtBox, BorderLayout.CENTER);
        extractWatermarkBtn = new JButton();
        extractWatermarkBtn.setEnabled(false);
        extractWatermarkBtn.setText("Extract and Decrypt");
        panel16.add(extractWatermarkBtn, new GridConstraints(3, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JPanel panel18 = new JPanel();
        panel18.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
        extractPanel.add(panel18, new GridConstraints(2, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, 1, 1, null, null, null, 0, false));
        extractBackBtn = new JButton();
        extractBackBtn.setText("Back");
        panel18.add(extractBackBtn, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_EAST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        comparePanel = new JPanel();
        comparePanel.setLayout(new GridLayoutManager(2, 1, new Insets(0, 0, 0, 0), -1, -1));
        mainPanel.add(comparePanel, "compare");
        final JPanel panel19 = new JPanel();
        panel19.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
        comparePanel.add(panel19, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, 1, null, null, null, 0, false));
        final JLabel label9 = new JLabel();
        label9.setFont(new Font(label9.getFont().getName(), Font.BOLD | Font.ITALIC, 36));
        label9.setHorizontalAlignment(0);
        label9.setHorizontalTextPosition(0);
        label9.setText("Compare Images");
        panel19.add(label9);
        final JPanel panel20 = new JPanel();
        panel20.setLayout(new GridLayoutManager(2, 1, new Insets(0, 0, 0, 0), -1, -1));
        comparePanel.add(panel20, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        final JPanel panel21 = new JPanel();
        panel21.setLayout(new BorderLayout(0, 0));
        panel20.add(panel21, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        final JPanel panel22 = new JPanel();
        panel22.setLayout(new GridLayoutManager(2, 1, new Insets(0, 0, 15, 5), -1, -1));
        panel21.add(panel22, BorderLayout.WEST);
        cmpImage1Lbl = new JLabel();
        cmpImage1Lbl.setText("");
        panel22.add(cmpImage1Lbl, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, new Dimension(200, 200), null, null, 0, false));
        cmpSelectImage1Btn = new JButton();
        cmpSelectImage1Btn.setText("Select Image 1");
        panel22.add(cmpSelectImage1Btn, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JPanel panel23 = new JPanel();
        panel23.setLayout(new GridLayoutManager(2, 1, new Insets(0, 0, 15, 0), -1, -1));
        panel21.add(panel23, BorderLayout.EAST);
        cmpImage2Lbl = new JLabel();
        cmpImage2Lbl.setText("");
        panel23.add(cmpImage2Lbl, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, new Dimension(200, 200), null, null, 0, false));
        cmpSelectImage2Btn = new JButton();
        cmpSelectImage2Btn.setText("Select Image 2");
        panel23.add(cmpSelectImage2Btn, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JPanel panel24 = new JPanel();
        panel24.setLayout(new GridLayoutManager(4, 1, new Insets(0, 0, 0, 0), -1, -1));
        panel21.add(panel24, BorderLayout.SOUTH);
        final JPanel panel25 = new JPanel();
        panel25.setLayout(new GridLayoutManager(1, 1, new Insets(5, 0, 5, 0), -1, 5));
        panel24.add(panel25, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        compareImagesBtn = new JButton();
        compareImagesBtn.setEnabled(false);
        compareImagesBtn.setText("Compare Images");
        panel25.add(compareImagesBtn, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE, 1, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JPanel panel26 = new JPanel();
        panel26.setLayout(new BorderLayout(0, 0));
        panel24.add(panel26, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE, 1, 1, new Dimension(200, -1), null, new Dimension(200, -1), 0, false));
        final JLabel label10 = new JLabel();
        label10.setHorizontalAlignment(2);
        label10.setHorizontalTextPosition(2);
        label10.setText("MSE Value:");
        panel26.add(label10, BorderLayout.WEST);
        mseLbl = new JLabel();
        mseLbl.setHorizontalAlignment(4);
        mseLbl.setHorizontalTextPosition(4);
        mseLbl.setText("-");
        panel26.add(mseLbl, BorderLayout.EAST);
        final JPanel panel27 = new JPanel();
        panel27.setLayout(new BorderLayout(0, 0));
        panel24.add(panel27, new GridConstraints(2, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE, 1, 1, new Dimension(200, -1), null, new Dimension(200, -1), 0, false));
        final JLabel label11 = new JLabel();
        label11.setHorizontalAlignment(2);
        label11.setHorizontalTextPosition(2);
        label11.setText("SNR");
        panel27.add(label11, BorderLayout.WEST);
        snrLbl = new JLabel();
        snrLbl.setHorizontalAlignment(4);
        snrLbl.setHorizontalTextPosition(4);
        snrLbl.setText("-");
        panel27.add(snrLbl, BorderLayout.EAST);
        final JPanel panel28 = new JPanel();
        panel28.setLayout(new BorderLayout(0, 0));
        panel24.add(panel28, new GridConstraints(3, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE, 1, 1, new Dimension(200, -1), null, new Dimension(200, -1), 0, false));
        final JLabel label12 = new JLabel();
        label12.setHorizontalAlignment(2);
        label12.setHorizontalTextPosition(2);
        label12.setText("PSNR Value:");
        panel28.add(label12, BorderLayout.WEST);
        psnrLbl = new JLabel();
        psnrLbl.setHorizontalAlignment(4);
        psnrLbl.setHorizontalTextPosition(4);
        psnrLbl.setText("-");
        panel28.add(psnrLbl, BorderLayout.EAST);
        final JPanel panel29 = new JPanel();
        panel29.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
        panel20.add(panel29, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, 1, 1, null, null, null, 0, false));
        cmpBackBtn = new JButton();
        cmpBackBtn.setText("Back");
        panel29.add(cmpBackBtn, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_EAST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        ButtonGroup buttonGroup;
        buttonGroup = new ButtonGroup();
        buttonGroup.add(imageWatermarkRb);
        buttonGroup.add(textWatermarkRb);
    }
}
