package pt.up.fc.dcc.exceedw.utils;

/**
 * Utilities for converting from/to binary arrays
 *
 * @author José Carlos Paiva    <josepaiva94@gmail.com>
 * @author Adélcia Fernandes    <up201308701@fc.up.pt>
 * @author Francisco Sucena     <up201102753@fc.up.pt>
 * @author Pedro Seruca         <up201109026@fc.up.pt>
 */
public class ByteUtils {

    /**
     * Convert from int to a byte array
     *
     * @param n the number to convert
     * @return byte array
     */
    public static byte[] fromInt(int n) {

        return new byte[]{
                (byte) ((n & 0xFF000000) >>> 24),
                (byte) ((n & 0x00FF0000) >>> 16),
                (byte) ((n & 0x0000FF00) >>> 8),
                (byte)  (n & 0x000000FF)
        };
    }

    /**
     * Convert from byte array to a int
     *
     * @param bs the byte array to convert
     * @return int
     */
    public static int toInt(byte[] bs) {

        return  ((bs[0] << 24) & 0xFF000000) |
                ((bs[1] << 16) & 0x00FF0000) |
                ((bs[2] << 8 ) & 0x0000FF00) |
                 (bs[3]        & 0x000000FF);
    }

    /**
     * Get one bit from a byte array
     *
     * @param data byte array
     * @param pos  position of the bit
     * @return bit from a data in position pos
     */
    public static int getBit(byte[] data, int pos) {
        int posByte = pos / 8;
        int posBit = pos % 8;
        byte valByte = data[posByte];
        return (valByte >> (8 - (posBit + 1))) & 0x0001;
    }
}
