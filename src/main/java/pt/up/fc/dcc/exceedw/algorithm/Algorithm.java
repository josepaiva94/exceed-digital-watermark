package pt.up.fc.dcc.exceedw.algorithm;

import pt.up.fc.dcc.exceedw.utils.DigitalImage;

/**
 * Interface of an algorithm for digital watermarking
 *
 * @author José Carlos Paiva    <josepaiva94@gmail.com>
 * @author Adélcia Fernandes    <up201308701@fc.up.pt>
 * @author Francisco Sucena     <up201102753@fc.up.pt>
 * @author Pedro Seruca         <up201109026@fc.up.pt>
 */
public interface Algorithm {
    String IMAGE_WATERMARK         = "!IMG!";
    String STRING_WATERMARK        = "!STR!";
    String CHARSET                 = "UTF-8";
    int BYTE_BIT_LENGTH            = 8;
    int INTEGER_BYTE_LENGTH        = 4;
    int WATERMARK_TYPE_BYTE_LENGTH = Math.max(IMAGE_WATERMARK.getBytes().length, STRING_WATERMARK.getBytes().length);

    /**
     * Embed image watermark in a image
     * @param in source image
     * @param watermark message image
     * @return encrypted image
     */
    EmbedResult embed(DigitalImage in, DigitalImage watermark) throws EmbedException;

    /**
     * Embed text watermark in a image
     * @param in source image
     * @param watermark message text
     * @return encrypted image
     */
    EmbedResult embed(DigitalImage in, String watermark) throws EmbedException;

    /**
     * Extract watermark from an image
     * @param in encrypted image
     * @return watermark
     */
    ExtractResult extract(DigitalImage in, String secretCode) throws ExtractException;

    /**
     * Result of embedding a watermark (image & secret code)
     */
    class EmbedResult {
        private DigitalImage image;
        private String secretCode;

        public EmbedResult() {
        }

        public EmbedResult(DigitalImage image, String secretCode) {
            this.image = image;
            this.secretCode = secretCode;
        }

        public DigitalImage getImage() {
            return image;
        }

        public void setImage(DigitalImage image) {
            this.image = image;
        }

        public String getSecretCode() {
            return secretCode;
        }

        public void setSecretCode(String secretCode) {
            this.secretCode = secretCode;
        }
    }

    /**
     * Result of extracting a watermark (text or digital image)
     */
    enum ExtractResult {

        TextType, DigitalImageType;

        String text;
        DigitalImage digitalImage;

        @SuppressWarnings("unchecked")
        public <T> T returnTextOrImage() {
            switch (this) {
                case TextType:
                    return (T) text;
                case DigitalImageType:
                    return (T) digitalImage;
            }

            return null;
        }

        public ExtractResult returnTextOrImage(Object o) {
            switch (this) {
                case TextType:
                    text = String.valueOf(o);
                    break;
                case DigitalImageType:
                    digitalImage = (DigitalImage) o;
                    break;
            }

            return this;
        }
    }
}
