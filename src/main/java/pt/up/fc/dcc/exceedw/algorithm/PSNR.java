package pt.up.fc.dcc.exceedw.algorithm;

import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * Peak signal-to-noise ratio (PSNR) is the ratio between the maximum possible power of a signal and the power of
 * corrupting noise that affects the fidelity of its representation. PSNR is usually expressed in terms of the
 * logarithmic decibel scale.
 *
 * @author José Carlos Paiva    <josepaiva94@gmail.com>
 * @author Adélcia Fernandes    <up201308701@fc.up.pt>
 * @author Francisco Sucena     <up201102753@fc.up.pt>
 * @author Pedro Seruca         <up201109026@fc.up.pt>
 */
public class PSNR implements Evaluation {
    private static final int NUMBER_COLOR_COMPONENTS = 4;
    private static final double MINIMUM_ERROR_THRESHOLD = 0.00001;

    private BufferedImage original = null;
    private BufferedImage watermarked = null;
    private double mse = 0.0;
    private double snr = 0.0;
    private double psnr = 0.0;

    public PSNR(BufferedImage original, BufferedImage watermarked) {
        this.original = original;
        this.watermarked = watermarked;
    }

    @Override
    public void evaluate() throws EvaluationException {

        if (original.getWidth() != watermarked.getWidth() || original.getHeight() != watermarked.getHeight())
            throw new EvaluationException("Images should have same dimensions");

        double signal = 0, noise = 0;

        for (int x = 0; x < original.getWidth(); x++) {

            for (int y = 0; y < original.getHeight(); y++) {

                Color colorOriginal = new Color(original.getRGB(x, y));
                Color colorWatermarked = new Color(watermarked.getRGB(x, y));

                double sqDiffRed = Math.pow(colorOriginal.getRed() - colorWatermarked.getRed(), 2);
                double sqDiffGreen = Math.pow(colorOriginal.getGreen() - colorWatermarked.getGreen(), 2);
                double sqDiffBlue = Math.pow(colorOriginal.getBlue() - colorWatermarked.getBlue(), 2);
                double sqDiffAlpha = Math.pow(colorOriginal.getAlpha() - colorWatermarked.getAlpha(), 2);

                int sumColorComps = colorOriginal.getRed() + colorOriginal.getGreen() + colorOriginal.getBlue()
                        + colorOriginal.getAlpha();

                signal += Math.pow(sumColorComps, 2);
                noise += sqDiffRed + sqDiffGreen + sqDiffBlue + sqDiffAlpha;
            }
        }

        mse = noise / (double) (original.getWidth() * original.getHeight() * NUMBER_COLOR_COMPONENTS);

        if (mse <= MINIMUM_ERROR_THRESHOLD)
            throw new EvaluationException("Images are equal, PSNR is infinity");

        snr = 10 * log10(signal / noise);
        psnr = 10 * log10(255 * 255 / mse);
    }

    public double getMse() {
        return mse;
    }

    public double getSnr() {
        return snr;
    }

    public double getPsnr() {
        return psnr;
    }

    private static double log10(double x) {
        return Math.log(x) / Math.log(10);
    }
}
