package pt.up.fc.dcc.exceedw.utils;

import javax.swing.*;
import java.awt.*;

/**
 * Utilities to display alert messages to the user
 *
 * @author José Carlos Paiva    <josepaiva94@gmail.com>
 * @author Adélcia Fernandes    <up201308701@fc.up.pt>
 * @author Francisco Sucena     <up201102753@fc.up.pt>
 * @author Pedro Seruca         <up201109026@fc.up.pt>
 */
public class Alerts {

    /**
     * Show a warning message
     * @param msg
     */
    public static void showWarningMsg(String msg) {
        Toolkit.getDefaultToolkit().beep();
        JOptionPane optionPane = new JOptionPane(msg, JOptionPane.WARNING_MESSAGE);
        JDialog dialog = optionPane.createDialog("Warning!");
        dialog.setAlwaysOnTop(true);
        dialog.setVisible(true);
    }

    /**
     * Show an error message
     * @param msg
     */
    public static void showErrorMsg(String msg) {
        Toolkit.getDefaultToolkit().beep();
        JOptionPane optionPane = new JOptionPane(msg, JOptionPane.ERROR_MESSAGE);
        JDialog dialog = optionPane.createDialog("Error!");
        dialog.setAlwaysOnTop(true);
        dialog.setVisible(true);
    }

    /**
     * Show a message
     * @param msg
     */
    public static void showMsg(String msg) {
        showMsg(msg, "Message");
    }

    /**
     * Show a message
     * @param msg
     * @param title
     */
    public static void showMsg(String msg, String title) {
        Toolkit.getDefaultToolkit().beep();
        JOptionPane optionPane = new JOptionPane(msg, JOptionPane.INFORMATION_MESSAGE);
        JDialog dialog = optionPane.createDialog(title);
        dialog.setAlwaysOnTop(true);
        dialog.setVisible(true);
    }
}
