package pt.up.fc.dcc.exceedw.algorithm;

/**
 * This type of exception occurs while extracting a watermark
 *
 * @author José Carlos Paiva    <josepaiva94@gmail.com>
 * @author Adélcia Fernandes    <up201308701@fc.up.pt>
 * @author Francisco Sucena     <up201102753@fc.up.pt>
 * @author Pedro Seruca         <up201109026@fc.up.pt>
 */
public class EmbedException extends Exception {

    EmbedException() {
    }

    EmbedException(String message) {
        super(message);
    }
}
