package pt.up.fc.dcc.exceedw.algorithm;

/**
 * This type of exception occurs while evaluating the performance of the algorithm
 *
 * @author José Carlos Paiva    <josepaiva94@gmail.com>
 * @author Adélcia Fernandes    <up201308701@fc.up.pt>
 * @author Francisco Sucena     <up201102753@fc.up.pt>
 * @author Pedro Seruca         <up201109026@fc.up.pt>
 */
public class EvaluationException extends Exception {

    EvaluationException() {
    }

    EvaluationException(String message) {
        super(message);
    }
}
