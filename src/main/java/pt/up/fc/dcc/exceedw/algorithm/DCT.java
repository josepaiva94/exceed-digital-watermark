package pt.up.fc.dcc.exceedw.algorithm;

import pt.up.fc.dcc.exceedw.utils.ByteUtils;
import pt.up.fc.dcc.exceedw.utils.CryptoUtils;
import pt.up.fc.dcc.exceedw.utils.DigitalImage;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Arrays;

/**
 * Discrete Cosine Transform (DCT) watermarking algorithm implementation for encrypting
 * or decrypting a watermark from an image. DCT is a transform domain algorithm.
 *
 * @author José Carlos Paiva    <josepaiva94@gmail.com>
 * @author Adélcia Fernandes    <up201308701@fc.up.pt>
 * @author Francisco Sucena     <up201102753@fc.up.pt>
 * @author Pedro Seruca         <up201109026@fc.up.pt>
 */
public class DCT /*implements Algorithm */{
    /*private final static int BLOCK_WIDTH = 8;

    private double[] coeffs;

    public DCT() {
        initCoefficients();
    }

    *//**
     * Initialize coefficients
     *//*
    private void initCoefficients() {
        coeffs = new double[size];

        for (int i = 1; i < size; i++) {
            coeffs[i] = 1;
        }

        coeffs[0] = 1 / Math.sqrt(2.0);
    }

    public EmbedResult embed(DigitalImage in, DigitalImage watermark) throws EmbedException {

        // get bytes for type
        byte[] type = IMAGE_WATERMARK.getBytes();

        // get bytes for size
        byte[] width = ByteUtils.fromInt(watermark.getWidth());
        byte[] height = ByteUtils.fromInt(watermark.getHeight());

        // get bytes for image
        byte[] message;
        try {
            message = watermark.toByteArray();
        } catch (IOException e) {
            throw new EmbedException("Converting buffered image to byte array");
        }

        // combine type and message
        int offset = 0;
        byte[] messageWithType = new byte[type.length + width.length + height.length + message.length];

        System.arraycopy(type, 0, messageWithType, offset, type.length);
        offset += type.length;

        System.arraycopy(width, 0, messageWithType, offset, width.length);
        offset += width.length;

        System.arraycopy(height, 0, messageWithType, offset, height.length);
        offset += height.length;

        System.arraycopy(message, 0, messageWithType, offset, message.length);

        return embed(in, messageWithType);
    }

    public EmbedResult embed(DigitalImage in, String watermark) throws EmbedException {

        // get bytes for type
        byte[] type = STRING_WATERMARK.getBytes();

        // get bytes for string
        byte[] message = watermark.getBytes();

        // combine type and message
        byte[] messageWithType = new byte[type.length + message.length];
        System.arraycopy(type, 0, messageWithType, 0, type.length);
        System.arraycopy(message, 0, messageWithType, type.length, message.length);

        return embed(in, messageWithType);
    }

    public ExtractResult extract(DigitalImage in, String secretCode) throws ExtractException {

        byte[] inBytes;
        try {
            inBytes = in.toByteArray();
        } catch (IOException e) {
            throw new ExtractException(e.getMessage());
        }

        int offset = 0;

        int length;
        try {
            length = getLength(inBytes, offset);
            offset += INTEGER_BYTE_LENGTH * BYTE_BIT_LENGTH;
        } catch (Exception e) {
            throw new ExtractException(e.getMessage());
        }

        byte[] decodedMsgBytes;
        try {
            decodedMsgBytes = getDecodedMessage(inBytes, length, offset, secretCode); // decrypted bytes of the message
            offset = 0; // reset offset
        } catch (Exception e) {
            throw new ExtractException(e.getMessage());
        }

        String type;
        try {
            type = getType(decodedMsgBytes, offset);
            offset += WATERMARK_TYPE_BYTE_LENGTH;
        } catch (Exception e) {
            throw new ExtractException(e.getMessage());
        }

        if (type.equals(IMAGE_WATERMARK)) {

            int width;
            try {
                width = getWidth(decodedMsgBytes, offset);
                offset += INTEGER_BYTE_LENGTH;
            } catch (Exception e) {
                throw new ExtractException(e.getMessage());
            }

            int height;
            try {
                height = getHeight(decodedMsgBytes, offset);
                offset += INTEGER_BYTE_LENGTH;
            } catch (Exception e) {
                throw new ExtractException(e.getMessage());
            }

            BufferedImage img = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);

            int[] argbData = new int[width * height];
            for (int i = 0, k = offset; i < argbData.length; i++, k = k + INTEGER_BYTE_LENGTH) {
                argbData[i] = ByteUtils.toInt(Arrays.copyOfRange(decodedMsgBytes, k, k + INTEGER_BYTE_LENGTH));
            }

            img.setRGB(0, 0, width, height, argbData, 0, width);

            DigitalImage watermark;
            try {
                watermark = new DigitalImage(img);
            } catch (IOException e) {
                throw new ExtractException("Recreating watermark image: " + e.getMessage());
            }

            return ExtractResult.DigitalImageType.returnTextOrImage(watermark);
        } else {

            String watermark;
            try {
                watermark = new String(decodedMsgBytes, offset, decodedMsgBytes.length - offset, CHARSET);
            } catch (Exception e) {
                throw new ExtractException("Reading watermark text: " + e.getMessage());
            }

            return ExtractResult.TextType.returnTextOrImage(watermark);
        }
    }

    *//**
     * Embed byte sequence of watermark in a image
     * @param in source image
     * @param watermark byte sequence
     * @return encrypted image
     *//*
    private EmbedResult embed(DigitalImage in, byte[] watermark) throws EmbedException {

        CryptoUtils.CryptoResult cryptoResult;
        try {
            cryptoResult = CryptoUtils.encrypt(watermark);
        } catch (GeneralSecurityException e) {
            throw new EmbedException(e.getMessage());
        }

        byte[] encryptedWatermark = buildMessage(cryptoResult.getEncryptedData());

        int messageBitLength = encryptedWatermark.length * 8;

        byte[] inBytes;
        try {
            inBytes = in.toByteArray();
        } catch (IOException e) {
            throw new EmbedException(e.getMessage());
        }

        int sourceLsbLength = inBytes.length;

        if (sourceLsbLength < messageBitLength)
            throw new EmbedException("Image is too small to embed this watermark");

        double dct[][] = new double[in.getHeight()][in.getWidth()];
       // double dct[][] = new double[in.getHeight()][in.getWidth()];

        return new EmbedResult(in, cryptoResult.getSecretCode());
    }

    *//**
     * Build message with length
     * @param message message
     * @return message with length
     *//*
    private static byte[] buildMessage(byte[] message) {

        byte[] lenBytes = ByteUtils.fromInt(message.length);

        int totalLen = lenBytes.length + message.length;
        byte[] messageWithLength = new byte[totalLen];

        System.arraycopy(lenBytes, 0, messageWithLength, 0, lenBytes.length);
        System.arraycopy(message, 0, messageWithLength, lenBytes.length, message.length);

        return messageWithLength;
    }

    *//**
     * Get length the message
     * @param inBytes source bytes
     * @param offset starting byte
     * @return length the message
     *//*
    private static int getLength(byte[] inBytes, int offset) throws Exception {

        byte[] lenBytes = extractBytes(inBytes, INTEGER_BYTE_LENGTH, offset);

        int length = ByteUtils.toInt(lenBytes);

        if (length <= 0 || length > inBytes.length)
            throw new Exception("Length of message is corrupted");

        return length;
    }

    *//**
     * Get decoded message bytes
     * @param inBytes source bytes
     * @param size length in bytes of encoded message
     * @param offset starting byte
     * @return decoded message bytes
     *//*
    private static byte[] getDecodedMessage(byte[] inBytes, int size, int offset, String secretCode)
            throws Exception {

        byte[] encMessageBytes = extractBytes(inBytes, size, offset);

        byte[] decodedMessageBytes = CryptoUtils.decrypt(encMessageBytes, secretCode);

        return decodedMessageBytes;
    }

    *//**
     * Get type of the message
     * @param inBytes source bytes
     * @param offset starting byte
     * @return type of the message
     *//*
    private static String getType(byte[] inBytes, int offset) throws Exception {

        byte[] typeBytes = Arrays.copyOfRange(inBytes, offset, WATERMARK_TYPE_BYTE_LENGTH);

        String type = new String(typeBytes, CHARSET);

        if (!type.equals(IMAGE_WATERMARK) && !type.equals(STRING_WATERMARK))
            throw new Exception("Type of message is corrupted");

        return type;
    }

    *//**
     * Get width of the watermark
     * @param inBytes source bytes
     * @param offset starting byte
     * @return width of the watermark
     *//*
    private static int getWidth(byte[] inBytes, int offset) throws Exception {

        byte[] widthBytes = Arrays.copyOfRange(inBytes, offset, offset + INTEGER_BYTE_LENGTH);

        int width = ByteUtils.toInt(widthBytes);

        if (width < 0)
            throw new Exception("Width of the watermark corrupted");

        return width;
    }

    *//**
     * Get height of the watermark
     * @param inBytes source bytes
     * @param offset starting byte
     * @return height of the watermark
     *//*
    private static int getHeight(byte[] inBytes, int offset) throws Exception {

        byte[] heightBytes = Arrays.copyOfRange(inBytes, offset, offset + INTEGER_BYTE_LENGTH);

        int height = ByteUtils.toInt(heightBytes);

        if (height < 0)
            throw new Exception("Height of the watermark corrupted");

        return height;
    }

    *//**
     * Extract size bytes from inBytes starting at offset
     * @param inBytes source bytes
     * @param size number of bytes
     * @param offset starting byte
     * @return extracted bytes
     * @throws Exception when size of image exceeded
     *//*
    private static byte[] extractBytes(byte[] inBytes, int size, int offset) throws Exception {

        int finalPos = offset + (size * 8);

        if (finalPos > inBytes.length)
            throw new Exception("Size of image exceeded");

        byte[] extractedBytes = new byte[size];
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < 8; j++) {
                extractedBytes[i] = (byte) ((extractedBytes[i] << 1) | inBytes[offset] & 1);
                offset++;
            }
        }

        return extractedBytes;
    }

    private double[][] applyDCT(double[][] f) {
        int N = size;

        double[][] F = new double[N][N];
        for (int u = 0; u < N; u++) {
            for (int v = 0; v < N; v++) {
                double sum = 0.0;
                for (int i = 0; i < N; i++) {
                    for (int j = 0; j < N; j++) {
                        sum += Math.cos(((2 * i + 1) / (2.0 * N)) * u * Math.PI) * Math.cos(((2 * j + 1) / (2.0 * N)) * v * Math.PI)
                                * (f[i][j]);
                    }
                }
                sum *= ((2 * coeffs[u] * coeffs[v]) / Math.sqrt(BLOCK_WIDTH * BLOCK_WIDTH));
                F[u][v] = sum;
            }
        }
        return F;
    }*/
}
