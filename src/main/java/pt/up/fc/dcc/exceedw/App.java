package pt.up.fc.dcc.exceedw;

import pt.up.fc.dcc.exceedw.ui.controller.MainFrameController;

import javax.swing.*;

/**
 * Main class of the Application
 *
 * @author José Carlos Paiva    <josepaiva94@gmail.com>
 * @author Adélcia Fernandes    <up201308701@fc.up.pt>
 * @author Francisco Sucena     <up201102753@fc.up.pt>
 * @author Pedro Seruca         <up201109026@fc.up.pt>
 */
public class App extends JFrame {

    public static void main(String[] args) {
        MainFrameController controller = new MainFrameController();
        controller.showMainFrameWindow();
    }
}
