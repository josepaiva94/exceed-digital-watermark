

# How to run?

Run the compiled jar with `java -jar exceedw-1.0.0.jar`

# How to build?

Create a Maven run configuration with goals `clean compile install assembly:single`

